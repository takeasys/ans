<?php

/* stats.search.performancegrid.tpl */
class __TwigTemplate_018ea2cbb013a52a2fe77b14b92d9612 extends Twig_Template
{
    public function display(array $context)
    {
        // line 1
        echo "<a name=\"";
        echo twig_safe_filter((isset($context['Anchor']) ? $context['Anchor'] : null));
        echo "\"></a>
<div style=\"text-align:right; ";
        // line 2
        echo twig_safe_filter((isset($context['HidePagingLinks']) ? $context['HidePagingLinks'] : null));
        echo "\">
\t<div style=\"padding-bottom:10px\">
\t\t";
        // line 4
        echo getLang("ResultsPerPage");
        echo ":
\t\t<select onchange=\"";
        // line 5
        echo twig_safe_filter((isset($context['ChangePerPageFunction']) ? $context['ChangePerPageFunction'] : null));
        echo "(this.options[this.selectedIndex].value)\">
\t\t\t<option ";
        // line 6
        echo twig_safe_filter((isset($context['IsShowPerPage5']) ? $context['IsShowPerPage5'] : null));
        echo " value=\"5\">5</option>
\t\t\t<option ";
        // line 7
        echo twig_safe_filter((isset($context['IsShowPerPage10']) ? $context['IsShowPerPage10'] : null));
        echo " value=\"10\">10</option>
\t\t\t<option ";
        // line 8
        echo twig_safe_filter((isset($context['IsShowPerPage20']) ? $context['IsShowPerPage20'] : null));
        echo " value=\"20\">20</option>
\t\t\t<option ";
        // line 9
        echo twig_safe_filter((isset($context['IsShowPerPage30']) ? $context['IsShowPerPage30'] : null));
        echo " value=\"30\">30</option>
\t\t\t<option ";
        // line 10
        echo twig_safe_filter((isset($context['IsShowPerPage50']) ? $context['IsShowPerPage50'] : null));
        echo " value=\"50\">50</option>
\t\t\t<option ";
        // line 11
        echo twig_safe_filter((isset($context['IsShowPerPage100']) ? $context['IsShowPerPage100'] : null));
        echo " value=\"100\">100</option>
\t\t\t<option ";
        // line 12
        echo twig_safe_filter((isset($context['IsShowPerPage200']) ? $context['IsShowPerPage200'] : null));
        echo " value=\"200\">200</option>
\t\t</select>
\t</div>
\t";
        // line 15
        echo twig_safe_filter((isset($context['Paging']) ? $context['Paging'] : null));
        echo "
</div>
<br style=\"clear: both;\" />
<table width=\"100%\" border=0 cellspacing=1 cellpadding=5 class=\"text\">
\t<tr class=\"Heading3\">
\t\t<td nowrap align=\"left\" width=\"40%\">
\t\t\t";
        // line 21
        echo getLang("SearchTerms");
        echo " &nbsp;
\t\t\t";
        // line 22
        echo twig_safe_filter((isset($context['SortLinksSearchTerms']) ? $context['SortLinksSearchTerms'] : null));
        echo "
\t\t</td>
\t\t<td align=\"right\">
\t\t\t";
        // line 25
        echo getLang("NumberOfSearches");
        echo " &nbsp;
\t\t\t";
        // line 26
        echo twig_safe_filter((isset($context['SortLinksNumberOfSearches']) ? $context['SortLinksNumberOfSearches'] : null));
        echo "
\t\t</td>
\t\t<td align=\"right\">
\t\t\t";
        // line 29
        echo getLang("NumberOfResults");
        echo " &nbsp;
\t\t\t";
        // line 30
        echo twig_safe_filter((isset($context['SortLinksNumberOfResults']) ? $context['SortLinksNumberOfResults'] : null));
        echo "
\t\t</td>
\t\t<td align=\"right\">
\t\t\t";
        // line 33
        echo getLang("SearchLastPerformed");
        echo " &nbsp;
\t\t\t";
        // line 34
        echo twig_safe_filter((isset($context['SortLinksLastPerformed']) ? $context['SortLinksLastPerformed'] : null));
        echo "
\t\t</td>
\t</tr>
\t";
        // line 37
        echo twig_safe_filter((isset($context['ResultsGrid']) ? $context['ResultsGrid'] : null));
        echo "
</table>
";
        // line 39
        echo twig_safe_filter((isset($context['JumpTo']) ? $context['JumpTo'] : null));
        echo "
";
    }

}
