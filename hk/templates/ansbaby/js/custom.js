$(document).ready(function(){

  var img_url = $(".agile-banner.w3lshome > img").prop("src");
  $(".agile-banner.w3lshome").css("background-image", "url("+img_url+")");
  $(".agile-banner.w3lshome > img").hide();

  /**** banner ****/
  var img_url = $("#banner > img").prop("src");
  $("#banner").css("background-image", "url("+img_url+")");
  $("#banner > img").hide();

  /**** top padding ****/
  var nav_height = $(".navbar-fixed-top.w3ls-navbar").outerHeight();
  $(".top-padding").css("height", nav_height+"px");

  /*** breadcrunb ****/
  $('.Breadcrumb > ul').addClass("breadcrumb");

  /*** category content ****/
  $('.ProductList > li').css("width", "auto");
  $('.ProductList > li').css("height", "auto");
  $('.ProductList > li').addClass("card");

  $('.ProductList .ProductImage').css("width", "auto");
  $('.ProductList .ProductImage').css("height", "auto");

  $('.ProductList .ProductActionAdd > a').addClass("btn btn-warning")

  /** stars scrolling icon **/
  /*
    var defaults = {
    containerID: 'toTop', // fading element id
    containerHoverID: 'toTopHover', // fading element hover id
    scrollSpeed: 1200,
    easingType: 'linear'
    };
  */

  $().UItoTop({ easingType: 'easeOutQuart' });

  /** scrolling script **/
  $(".scroll").click(function(event){
    event.preventDefault();
    $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
  });


  /** checkout **/
  $(".billingButton").addClass("btn btn-warning");

  /*** POS ***/
  $(".data-lightbox").each( function(){
    //console.log("light");
    $(this).data("toggle", "lightbox");
    $(this).data("gallery", "pos-gallery");
  } )

  //$(document).on('click', '[data-toggle="lightbox"]', function(event) {
  $(document).on('click', '.data-lightbox', function(event) {
      //console.log("clicked");
      event.preventDefault();
      $(this).ekkoLightbox();
  });


  /*** alert style ****/
  $(".SuccessMessage").addClass("alert alert-success");
  $(".ErrorMessage").addClass("alert alert-danger");
  $(".InfoMessage").addClass("alert alert-info");



})
