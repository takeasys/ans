<?php

/* weight_range_header.tpl */
class __TwigTemplate_e8cd0a4f118e3b08f9e8af19ba42f6d6 extends Twig_Template
{
    public function display(array $context)
    {
        // line 1
        echo "
<script type=\"text/javascript\" charset=\"utf-8\">

\tfunction AddWeightRange(node)
\t{
\t\tvar newNode = \$(node).clone();
\t\tvar newVal = \$(newNode).find('input:eq(1)').val();

\t\t\$(newNode).find('input:first').val(newVal);
\t\t\$(newNode).find('input:gt(0)').val('');

\t\tvar oldName = \$('.WeightRanges:last input:first').attr('name');
\t\tvar oldParts = oldName.replace(/^.*_/, '');
\t\tvar oldParts = oldParts.replace(/\\]/, '');
\t\tvar newNum = parseInt(oldParts)+1;

\t\t\$(newNode).find('input').each(function() {
\t\t\tparts = \$(this).attr('name').split(/[_|\\[|\\]]/);
\t\t\t\$(this).attr('name', parts[0] + '_' + parts[1] + '[' + parts[2] + '_' + newNum + ']');
\t\t});

\t\t\$(node.parentNode).append(newNode);

\t\tShowCorrectLinks();
\t}

\tfunction RemoveWeightRange(node)
\t{
\t\tif (ConfirmRemove(node)) {
\t\t\t\$(node).remove();
\t\t\tShowCorrectLinks();
\t\t}
\t}
\tfunction ShowCorrectLinks()
\t{
\t\t\$('.WeightRanges:first a.remove').hide();
\t\t\$('.WeightRanges:gt(0) a.remove').show();
\t}

\tfunction ConfirmRemove(node)
\t{
\t\tvar canRemove = true;
\t\t\$(node).find('input').each(function() {
\t\t\tif (\$(this).val() != '') {
\t\t\t\tif (confirm(\"";
        // line 45
        echo getLang("ConfirmRemoveWeightRange");
        echo "\")) {
\t\t\t\t\tcanRemove = true;
\t\t\t\t} else {
\t\t\t\t\tcanRemove = false;
\t\t\t\t}
\t\t\t\treturn false;
\t\t\t}
\t\t});

\t\treturn canRemove;
\t}

\t\$(document).ready(function () {
\t\tShowCorrectLinks();
\t});

</script>

";
    }

}
