<?php
require_once(dirname(__FILE__).'/init.php');

function GetSession($var)
{
	if (isset($_SESSION[md5($GLOBALS['ShopPath'])][$var])) {
		return $_SESSION[md5($GLOBALS['ShopPath'])][$var];
	} else {
		return null;
	}
}


function SetSession($var, $val)
{
	$_SESSION[md5($GLOBALS['ShopPath'])][$var] = $val;
}

function UnsetSession($var)
{
	unset($_SESSION[md5($GLOBALS['ShopPath'])][$var]);
}

// Internal ping system to maintain session login for long viewed pages (editing etc.)
if (isset($_GET['ping'])) {
	die("pong");
}
/*
# License System 
$licensed = array('status' => FALSE, 'error' => 'License Validation Error');
@include_once('./lcx/lcx.php');
if(!$licensed['status']) {
	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11"><html  xml:lang="en" lang="en"><head>	<title>Control Panel</title>	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	<meta name="robots" content="noindex, nofollow" />	<style type="text/css">		@import url("Styles/styles.css");		@import url("Styles/tabmenu.css");		@import url("Styles/iselector.css");	</style>	<link rel="SHORTCUT ICON" href="favicon.ico" />	<!--[if IE]>	<style type="text/css">		@import url("Styles/ie.css");	</style>	<![endif]-->	<script type="text/javascript" src="../javascript/jquery.js?1"></script>	<script type="text/javascript" src="script/menudrop.js?1"></script>	<script type="text/javascript" src="script/common.js?1"></script>	<script type="text/javascript" src="../javascript/iselector.js?1"></script>	<script type="text/javascript" src="../javascript/thickbox.js?1"></script>	<link rel="stylesheet" href="Styles/thickbox.css?1" type="text/css" media="screen" /></head><body>	<div id="box">		<table><tr><td style="border:solid 2px #DDD; padding:20px; background-color:#FFF; width:300px">		<table>		  <tr>			<td class="Heading1">				<a href="index.php"><img id="logo" src="images/logo.gif" border="0" /></a>			</td>		  </tr>		  <tr>			<td style="padding:0 0 0 5px">EC Shop City E-Shop Licensing Error: </td>		  </tr>		  <tr>			<td style="padding:0 0 0 5px"><span style="color:red;font-size:13px;">'.$licensed['error'].'</span></td>		  </tr>		  <tr>			<td style="padding:0 0 0 5px"><br/>Please contact us if you have further enquires concerning the above problem</td>		  </tr>		  <tr>			<td>			</td>		  </tr>		</table>		</td></tr></table>	</div>	<script type="text/javascript">		function sizeBox() {			var w = $(window).width();			var h = document.getElementsByTagName("html")[0].clientHeight;			$("#box").css("position", "absolute");			$("#box").css("top", h/2-($("#box").height()/2)-50);			$("#box").css("left", w/2-($("#box").width()/2));		}		$(document).ready(function() {			sizeBox();			$("#username").focus();		});		$(window).resize(function() {			sizeBox();		});	</script></body></html>';
	exit();
}
unset($licensed);
*/
# License System -

// The main page handling class
$GLOBALS['ISC_CLASS_ADMIN_ENGINE'] = GetClass('ISC_ADMIN_ENGINE');
$GLOBALS['ISC_CLASS_ADMIN_ENGINE']->HandlePage();
