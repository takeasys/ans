<?php

/* weight_range_row.tpl */
class __TwigTemplate_574b608e50caec014349a0a5d8702b64 extends Twig_Template
{
    public function display(array $context)
    {
        // line 1
        echo "
<div id=\"\" class=\"WeightRanges\">
\t";
        // line 3
        echo getLang("ByWeightRowStart");
        echo " <input type=\"text\" name=\"shipping_byweight[lower_";
        echo twig_safe_filter((isset($context['POS']) ? $context['POS'] : null));
        echo "]\" value=\"";
        echo twig_safe_filter((isset($context['LOWER_VAL']) ? $context['LOWER_VAL'] : null));
        echo "\" id=\"lower_";
        echo twig_safe_filter((isset($context['POS']) ? $context['POS'] : null));
        echo "\" class=\"Field50 WeightRange LowerRange\">
\t";
        // line 4
        echo twig_safe_filter((isset($context['WeightMeasurement']) ? $context['WeightMeasurement'] : null));
        echo " ";
        echo getLang("ByWeightRowMiddle");
        echo " <input type=\"text\" name=\"shipping_byweight[upper_";
        echo twig_safe_filter((isset($context['POS']) ? $context['POS'] : null));
        echo "]\" value=\"";
        echo twig_safe_filter((isset($context['UPPER_VAL']) ? $context['UPPER_VAL'] : null));
        echo "\" id=\"upper_";
        echo twig_safe_filter((isset($context['POS']) ? $context['POS'] : null));
        echo "\" class=\"Field50 WeightRange UpperRange\">
\t";
        // line 5
        echo twig_safe_filter((isset($context['WeightMeasurement']) ? $context['WeightMeasurement'] : null));
        echo " ";
        echo getLang("ByWeightRowEnd");
        echo "
\t";
        // line 6
        echo twig_safe_filter((isset($context['CurrencyTokenLeft']) ? $context['CurrencyTokenLeft'] : null));
        echo " <input type=\"text\" name=\"shipping_byweight[cost_";
        echo twig_safe_filter((isset($context['POS']) ? $context['POS'] : null));
        echo "]\" value=\"";
        echo twig_safe_filter((isset($context['COST_VAL']) ? $context['COST_VAL'] : null));
        echo "\" id=\"cost_";
        echo twig_safe_filter((isset($context['POS']) ? $context['POS'] : null));
        echo "\" class=\"Field50 WeightRange RangeCost\"> ";
        echo twig_safe_filter((isset($context['CurrencyTokenRight']) ? $context['CurrencyTokenRight'] : null));
        echo "
\t<a href=\"#\" onclick=\"AddWeightRange(this.parentNode); return false;\" class=\"add\">Add</a>
\t<a href=\"#\" onclick=\"RemoveWeightRange(this.parentNode); return false;\" class=\"remove\">Remove</a>
</div>
";
    }

}
