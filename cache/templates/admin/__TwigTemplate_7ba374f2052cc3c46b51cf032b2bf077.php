<?php

/* emailintegration.fieldsyncform.modalcontent.tpl */
class __TwigTemplate_7ba374f2052cc3c46b51cf032b2bf077 extends Twig_Template
{
    public function display(array $context)
    {
        // line 1
        echo "<div id=\"emailIntegrationFieldSyncModal\" class=\"emailIntegrationFieldSyncForm\">
\t";
        // line 2
        if (twig_length_filter($this->env, (isset($context['listFields']) ? $context['listFields'] : null))) {
            echo "\t\t<p class=\"emailIntegrationFieldSyncModalIntro\">";
            // line 3
            echo getLang("FieldSyncFormIntro", array("provider" => $this->getAttribute((isset($context['module']) ? $context['module'] : null), "name", array(), "any")));
            // line 5
            echo "</p>

\t\t<table>
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<th>";
            // line 10
            echo getLang("FieldSyncFormMapThisCustomerData");
            echo "</th>
\t\t\t\t\t<th>";
            // line 11
            echo getLang("FieldSyncFormMapThisProviderField", array("provider" => $this->getAttribute((isset($context['module']) ? $context['module'] : null), "name", array(), "any")));
            // line 13
            echo "</th>
\t\t\t\t\t<th>&nbsp;</th>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody class=\"emailIntegrationFieldSyncFormContent\">
\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<select class=\"Field200\" disabled=\"disabled\">
\t\t\t\t\t\t\t<option>";
            // line 21
            echo getLang("EmailAddress");
            echo "</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<select class=\"Field200\" disabled=\"disabled\">
\t\t\t\t\t\t\t<option>";
            // line 26
            echo getLang("EmailAddress");
            echo "</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</td>
\t\t\t\t\t<td class=\"mapActions\">
\t\t\t\t\t\t<a href=\"#\" class=\"mapAdd\"><img src=\"images/addicon.png\" width=\"16\" height=\"16\" alt=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context['lang']) ? $context['lang'] : null), "Add", array(), "any"), "1");
            echo "\" /></a>
\t\t\t\t\t\t<img class=\"mapNoDelete\" src=\"images/delicon_gray.png\" width=\"16\" height=\"16\" alt=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context['lang']) ? $context['lang'] : null), "Delete", array(), "any"), "1");
            echo "\" />
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</tbody>
\t\t\t<tfoot style=\"display:none;\">
\t\t\t\t<tr class=\"emailIntegrationFieldSyncFormTemplate\">
\t\t\t\t\t<td>
\t\t\t\t\t\t<select class=\"Field200 mapLocal\">
\t\t\t\t\t\t\t<option value=\"\">";
            // line 39
            echo getLang("FieldSyncFormChooseAField");
            echo "</option>
\t\t\t\t\t\t\t";
            // line 40
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_iterator_to_array((isset($context['formFields']) ? $context['formFields'] : null));
            $countable = is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable);
            $length = $countable ? count($context['_seq']) : null;
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if ($countable) {
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context['groupLabel'] => $context['fields']) {
                echo "\t\t\t\t\t\t\t\t<option value=\"\" disabled=\"disabled\"></option>
\t\t\t\t\t\t\t\t<optgroup label=\"";
                // line 42
                echo twig_escape_filter($this->env, (isset($context['groupLabel']) ? $context['groupLabel'] : null), "1");
                echo "\">
\t\t\t\t\t\t\t\t\t";
                // line 43
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_iterator_to_array((isset($context['fields']) ? $context['fields'] : null));
                $countable = is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable);
                $length = $countable ? count($context['_seq']) : null;
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if ($countable) {
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context['fieldId'] => $context['field']) {
                    echo "\t\t\t\t\t\t\t\t\t\t<option value=\"";
                    // line 44
                    echo twig_escape_filter($this->env, (isset($context['fieldId']) ? $context['fieldId'] : null), "1");
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context['field']) ? $context['field'] : null), "description", array(), "any"), "1");
                    echo "</option>
\t\t\t\t\t\t\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if ($countable) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['fieldId'], $context['field'], $context['_parent'], $context['loop']);
                $context = array_merge($_parent, array_intersect_key($context, $_parent));
                // line 45
                echo "\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if ($countable) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['groupLabel'], $context['fields'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 47
            echo "\t\t\t\t\t\t</select>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<select class=\"Field200 mapProvider\">
\t\t\t\t\t\t\t<option value=\"\">";
            // line 52
            echo getLang("FieldSyncFormChooseAField");
            echo "</option>
\t\t\t\t\t\t\t<option value=\"\" disabled=\"disabled\"></option>
\t\t\t\t\t\t\t";
            // line 54
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_iterator_to_array((isset($context['listFields']) ? $context['listFields'] : null));
            $countable = is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable);
            $length = $countable ? count($context['_seq']) : null;
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if ($countable) {
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context['_key'] => $context['field']) {
                echo "\t\t\t\t\t\t\t\t<option value=\"";
                // line 55
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context['field']) ? $context['field'] : null), "provider_field_id", array(), "any"), "1");
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context['field']) ? $context['field'] : null), "name", array(), "any"), "1");
                echo "</option>
\t\t\t\t\t\t\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if ($countable) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 56
            echo "\t\t\t\t\t\t</select>
\t\t\t\t\t</td>
\t\t\t\t\t<td class=\"mapActions\">
\t\t\t\t\t\t<a href=\"#\" class=\"mapAdd\"><img src=\"images/addicon.png\" width=\"16\" height=\"16\" alt=\"";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context['lang']) ? $context['lang'] : null), "Add", array(), "any"), "1");
            echo "\" /></a>
\t\t\t\t\t\t<a href=\"#\" class=\"mapDelete\"><img src=\"images/delicon.png\" width=\"16\" height=\"16\" alt=\"";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context['lang']) ? $context['lang'] : null), "Delete", array(), "any"), "1");
            echo "\" /></a>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</tfoot>
\t\t</table>
\t";
        } else {
            // line 66
            echo "\t\t<p>";
            // line 67
            echo getLang("FieldSyncFormListNotFound_1");
            echo "</p>
\t\t<p>";
            // line 68
            echo getLang("FieldSyncFormListNotFound_2");
            echo "</p>
\t\t<p>";
            // line 69
            echo getLang("FieldSyncFormListNotFound_3");
            echo "</p>
\t";
        }
        // line 70
        echo "</div>

<script language=\"javascript\" type=\"text/javascript\">//<![CDATA[
(function(\$){
\t//\tpreloaded mappings
\tvar mappings = {};
\t";
        // line 77
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_iterator_to_array((isset($context['mappings']) ? $context['mappings'] : null));
        $countable = is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable);
        $length = $countable ? count($context['_seq']) : null;
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if ($countable) {
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context['providerField'] => $context['localField']) {
            echo "\t\tmappings[\"";
            // line 78
            echo $this->getEnvironment()->getExtension('interspire')->jsFilter((isset($context['providerField']) ? $context['providerField'] : null));
            echo "\"] = \"";
            echo $this->getEnvironment()->getExtension('interspire')->jsFilter((isset($context['localField']) ? $context['localField'] : null));
            echo "\";
\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if ($countable) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['providerField'], $context['localField'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 79
        echo "\t//\tend of preloaded mappings

\t\$(function(){
\t\tvar form = \$('.emailIntegrationFieldSyncForm');

\t\tform.delegate('.mapNoDelete', 'click', function(){
\t\t\talert(\"";
        // line 86
        echo Interspire_Template_Extension::jsFilter(getLang("FieldSyncFormEmailNoDelete"), "'");
        echo "\");
\t\t});

\t\t\$.each(mappings, function(providerField, localField){
\t\t\t// add a new row to work with
\t\t\tform.find('.mapAdd').eq(0).click();

\t\t\tvar row = \$('.emailIntegrationFieldSyncFormContent tr:last');
\t\t\trow.find('.mapLocal').val(localField);
\t\t\trow.find('.mapProvider').val(providerField);
\t\t});

\t\t// finally, add a blank row to help the user understand what to do next
\t\tform.find('.mapAdd').eq(0).click();

\t\tif (ReadCookie('fieldSyncFormGuessFields') != 'off') {
\t\t\t\$('#fieldSyncFormGuessFields').attr('checked', 'checked');
\t\t}

\t\t\$('#fieldSyncFormGuessFields').click(function(event){
\t\t\tif (this.checked) {
\t\t\t\tSetCookie('fieldSyncFormGuessFields', 'on');
\t\t\t} else {
\t\t\t\tSetCookie('fieldSyncFormGuessFields', 'off');
\t\t\t}
\t\t});
\t});

\tif (typeof lang == 'undefined') {
\t\tlang = {};
\t}

\tlang.FieldSyncFormDuplicateFieldsClientError = \"";
        // line 118
        echo Interspire_Template_Extension::jsFilter(getLang("FieldSyncFormDuplicateFieldsClientError", array("provider" => $this->getAttribute((isset($context['module']) ? $context['module'] : null), "name", array(), "any"))), "'");
        // line 120
        echo "\";

\tlang.FieldSyncFormMapUnmatchedFields = \"";
        // line 122
        echo Interspire_Template_Extension::jsFilter(getLang("FieldSyncFormMapUnmatchedFields"), "'");
        echo "\";
})(jQuery);
//]]></script>
";
    }

}
