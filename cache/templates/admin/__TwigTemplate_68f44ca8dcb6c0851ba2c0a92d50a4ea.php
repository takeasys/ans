<?php

/* weight_range_javascript.tpl */
class __TwigTemplate_68f44ca8dcb6c0851ba2c0a92d50a4ea extends Twig_Template
{
    public function display(array $context)
    {
        // line 1
        echo "var weightOk = true;

\$('.WeightRanges:first input:first').addClass('FirstWeight');
\$('.WeightRanges:last input:last').prev().addClass('LastWeight');


if (\$('.WeightRanges input.WeightRange').length > 3) {
\t\$('.WeightRanges input.WeightRange').each(function() {
\t\tif (\$(this).hasClass('FirstWeight') || \$(this).hasClass('LastWeight')) {
\t\t\treturn true;
\t\t}

\t\tif (isNaN(priceFormat(\$(this).val())) || \$(this).val() == \"\") {

\t\t\tif (\$(this).hasClass('RangeCost')) {
\t\t\t\talert('";
        // line 16
        echo getLang("JsEnterAShippingCost");
        echo "');
\t\t\t}

\t\t\t\$(this).focus();
\t\t\tweightOk = false;
\t\t\treturn false;
\t\t}
\t});
} else {
\tvar cost = \$('.WeightRanges input.RangeCost').val();
\tvar lower = \$('.WeightRanges input.LowerRange').val();
\tvar upper = \$('.WeightRanges input.UpperRange').val();

\tif (isNaN(priceFormat(cost)) || cost == \"\" ) {
\t\talert('";
        // line 30
        echo getLang("JsEnterAShippingCost");
        echo "');
\t\t\$('.WeightRanges input.RangeCost').focus();
\t\tweightOk = false;
\t} else if ((isNaN(priceFormat(lower)) || lower == \"\") && (isNaN(priceFormat(upper)) || upper == \"\")) {
\t\talert('";
        // line 34
        echo getLang("JsShippingCostRuleRequired");
        echo "');
\t\tweightOk = false;
\t}

}

if (weightOk == false) {
\t\$('.WeightRanges:first input:first').removeClass('FirstWeight');
\t\$('.WegihtRanges:last input:last').prev().removeClass('LastWeight');
\treturn false;
}


";
    }

}
