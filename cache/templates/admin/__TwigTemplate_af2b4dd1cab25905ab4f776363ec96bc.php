<?php

/* total_range_row.tpl */
class __TwigTemplate_af2b4dd1cab25905ab4f776363ec96bc extends Twig_Template
{
    public function display(array $context)
    {
        // line 1
        echo "
<div id=\"\" class=\"TotalRanges\">
\t";
        // line 3
        echo getLang("ByTotalRowStart");
        echo " ";
        echo twig_safe_filter((isset($context['CurrencyTokenLeft']) ? $context['CurrencyTokenLeft'] : null));
        echo " <input type=\"text\" name=\"shipping_bytotal[lower_";
        echo twig_safe_filter((isset($context['POS']) ? $context['POS'] : null));
        echo "]\" value=\"";
        echo twig_safe_filter((isset($context['LOWER_VAL']) ? $context['LOWER_VAL'] : null));
        echo "\" id=\"lower_";
        echo twig_safe_filter((isset($context['POS']) ? $context['POS'] : null));
        echo "\" class=\"Field50 TotalRange LowerRange\"> ";
        echo twig_safe_filter((isset($context['CurrencyTokenRight']) ? $context['CurrencyTokenRight'] : null));
        echo "
\t";
        // line 4
        echo twig_safe_filter((isset($context['TotalMeasurement']) ? $context['TotalMeasurement'] : null));
        echo " ";
        echo getLang("ByTotalRowMiddle");
        echo " ";
        echo twig_safe_filter((isset($context['CurrencyTokenLeft']) ? $context['CurrencyTokenLeft'] : null));
        echo " <input type=\"text\" name=\"shipping_bytotal[upper_";
        echo twig_safe_filter((isset($context['POS']) ? $context['POS'] : null));
        echo "]\" value=\"";
        echo twig_safe_filter((isset($context['UPPER_VAL']) ? $context['UPPER_VAL'] : null));
        echo "\" id=\"upper_";
        echo twig_safe_filter((isset($context['POS']) ? $context['POS'] : null));
        echo "\" class=\"Field50 TotalRange UpperRange\"> ";
        echo twig_safe_filter((isset($context['CurrencyTokenRight']) ? $context['CurrencyTokenRight'] : null));
        echo "
\t";
        // line 5
        echo twig_safe_filter((isset($context['TotalMeasurement']) ? $context['TotalMeasurement'] : null));
        echo " ";
        echo getLang("ByTotalRowEnd");
        echo "
\t";
        // line 6
        echo twig_safe_filter((isset($context['CurrencyTokenLeft']) ? $context['CurrencyTokenLeft'] : null));
        echo " <input type=\"text\" name=\"shipping_bytotal[cost_";
        echo twig_safe_filter((isset($context['POS']) ? $context['POS'] : null));
        echo "]\" value=\"";
        echo twig_safe_filter((isset($context['COST_VAL']) ? $context['COST_VAL'] : null));
        echo "\" id=\"cost_";
        echo twig_safe_filter((isset($context['POS']) ? $context['POS'] : null));
        echo "\" class=\"Field50 TotalRange RangeCost\"> ";
        echo twig_safe_filter((isset($context['CurrencyTokenRight']) ? $context['CurrencyTokenRight'] : null));
        echo "
\t<a href=\"#\" onclick=\"AddTotalRange(this.parentNode); return false;\" class=\"add\">Add</a>
\t<a href=\"#\" onclick=\"RemoveTotalRange(this.parentNode); return false;\" class=\"remove\">Remove</a>
</div>
";
    }

}
