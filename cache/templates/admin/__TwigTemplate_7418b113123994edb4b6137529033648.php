<?php

/* settings.returns.manage.tpl */
class __TwigTemplate_7418b113123994edb4b6137529033648 extends Twig_Template
{
    public function display(array $context)
    {
        // line 1
        echo "\t<form action=\"index.php?ToDo=saveUpdatedReturnsSettings\" name=\"frmReturnsSettings\" id=\"frmReturnsSettings\" method=\"post\" onsubmit=\"return ValidateForm(CheckReturnsSettingsForm)\">
\t\t<div class=\"BodyContainer\">
\t\t<table cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" style=\"margin-left: 4px; margin-top: 8px;\">
\t\t<tr>
\t\t\t<td class=\"Heading1\">";
        // line 5
        echo getLang("ReturnsSettings");
        echo "</td>
\t\t</tr>
\t\t<tr>
\t\t\t<td class=\"Intro\">
\t\t\t\t<p>";
        // line 9
        echo getLang("ReturnsSettingsIntro");
        echo "</p>
\t\t\t\t";
        // line 10
        echo twig_safe_filter((isset($context['Message']) ? $context['Message'] : null));
        echo "
\t\t\t\t<p>
\t\t\t\t<input type=\"submit\" value=\"";
        // line 12
        echo getLang("Save");
        echo "\" class=\"FormButton\" />
\t\t\t\t<input type=\"reset\" value=\"";
        // line 13
        echo getLang("Cancel");
        echo "\" class=\"FormButton\" onclick=\"ConfirmCancel()\" />
\t\t\t\t</p>
\t\t\t</td>
\t\t</tr>
\t\t<tr>
\t\t\t<td>
\t\t\t\t<table width=\"100%\" class=\"Panel\">
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td class=\"Heading2\" colspan=\"2\">";
        // line 21
        echo getLang("ReturnsSettings");
        echo "</td>
\t\t\t\t\t</tr>

\t\t\t\t\t<tr>
\t\t\t\t\t\t<td class=\"FieldLabel\">
\t\t\t\t\t\t\t";
        // line 26
        echo getLang("EnableReturnsSystem");
        echo "\t\t\t\t\t\t</td>
\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t<label><input type=\"checkbox\" name=\"enablereturns\" id=\"enablereturns\" value=\"1\" ";
        // line 29
        echo twig_safe_filter((isset($context['IsEnableReturns']) ? $context['IsEnableReturns'] : null));
        echo " onclick=\"ToggleReturnsStatus(this.checked)\" /> ";
        echo getLang("YesEnableReturnsSystem");
        echo "</label>
\t\t\t\t\t\t\t<img onmouseout=\"HideHelp('returns1');\" onmouseover=\"ShowHelp('returns1', '";
        // line 30
        echo getLang("EnableReturnsSystem");
        echo "', '";
        echo getLang("EnableReturnsHelp");
        echo "')\" src=\"images/help.gif\" width=\"24\" height=\"16\" border=\"0\">
\t\t\t\t\t\t\t<div style=\"display:none\" id=\"returns1\"></div>
\t\t\t\t\t\t</td>
\t\t\t\t\t</tr>

\t\t\t\t\t<tr class=\"HideOnDisabled\">
\t\t\t\t\t\t<td class=\"FieldLabel\">
\t\t\t\t\t\t\t";
        // line 37
        echo getLang("ReturnInstructions");
        echo ":
\t\t\t\t\t\t</td>
\t\t\t\t\t\t<td style=\"padding-left: 25px;\">
\t\t\t\t\t\t\t<textarea name=\"returninstructions\" id=\"returninstructions\" class=\"Field300\" rows=\"6\">";
        // line 40
        echo twig_safe_filter((isset($context['ReturnInstructions']) ? $context['ReturnInstructions'] : null));
        echo "</textarea>
\t\t\t\t\t\t\t<img onmouseout=\"HideHelp('returns2');\" onmouseover=\"ShowHelp('returns2', '";
        // line 41
        echo getLang("ReturnInstructions");
        echo "', '";
        echo getLang("ReturnInstructionsHelp");
        echo "')\" src=\"images/help.gif\" width=\"24\" height=\"16\" border=\"0\">
\t\t\t\t\t\t\t<div style=\"display:none\" id=\"returns2\"></div>
\t\t\t\t\t\t</td>
\t\t\t\t\t</tr>

\t\t\t\t\t<tr class=\"HideOnDisabled\">
\t\t\t\t\t\t<td class=\"FieldLabel\">
\t\t\t\t\t\t\t";
        // line 48
        echo getLang("ReturnReasons");
        echo ":
\t\t\t\t\t\t</td>
\t\t\t\t\t\t<td style=\"padding-left: 25px;\">
\t\t\t\t\t\t\t<textarea name=\"returnreasons\" id=\"returnreasons\" class=\"Field300\" rows=\"6\">";
        // line 51
        echo twig_safe_filter((isset($context['ReturnReasonsArea']) ? $context['ReturnReasonsArea'] : null));
        echo "</textarea>
\t\t\t\t\t\t\t<img onmouseout=\"HideHelp('returns3');\" onmouseover=\"ShowHelp('returns3', '";
        // line 52
        echo getLang("ReturnReasons");
        echo "', '";
        echo getLang("ReturnReasonsHelp");
        echo "')\" src=\"images/help.gif\" width=\"24\" height=\"16\" border=\"0\">
\t\t\t\t\t\t\t<div style=\"display:none\" id=\"returns3\"></div>
\t\t\t\t\t\t</td>
\t\t\t\t\t</tr>

\t\t\t\t\t<tr class=\"HideOnDisabled\">
\t\t\t\t\t\t<td class=\"FieldLabel\">
\t\t\t\t\t\t\t";
        // line 59
        echo getLang("ReturnActions");
        echo ":
\t\t\t\t\t\t</td>
\t\t\t\t\t\t<td style=\"padding-left: 25px;\">
\t\t\t\t\t\t\t<textarea name=\"returnactions\" id=\"returnactions\" class=\"Field300\" rows=\"6\">";
        // line 62
        echo twig_safe_filter((isset($context['ReturnActionsArea']) ? $context['ReturnActionsArea'] : null));
        echo "</textarea>
\t\t\t\t\t\t\t<img onmouseout=\"HideHelp('returns4');\" onmouseover=\"ShowHelp('returns4', '";
        // line 63
        echo getLang("ReturnActions");
        echo "', '";
        echo getLang("ReturnActionsHelp");
        echo "')\" src=\"images/help.gif\" width=\"24\" height=\"16\" border=\"0\">
\t\t\t\t\t\t\t<div style=\"display:none\" id=\"returns4\"></div>
\t\t\t\t\t\t</td>
\t\t\t\t\t</tr>

\t\t\t\t\t<tr class=\"HideOnDisabled PanelBottom\">
\t\t\t\t\t\t<td class=\"FieldLabel\">
\t\t\t\t\t\t\t";
        // line 70
        echo getLang("ReturnCredits");
        echo "\t\t\t\t\t\t</td>
\t\t\t\t\t\t<td style=\"padding-left: 25px;\">
\t\t\t\t\t\t\t<label><input type=\"checkbox\" name=\"returncredits\" id=\"returncredits\" value=\"1\" ";
        // line 73
        echo twig_safe_filter((isset($context['IsReturnCredits']) ? $context['IsReturnCredits'] : null));
        echo " /> ";
        echo getLang("YesEnableReturnCredits");
        echo "</label>
\t\t\t\t\t\t\t<img onmouseout=\"HideHelp('returns5');\" onmouseover=\"ShowHelp('returns5', '";
        // line 74
        echo getLang("ReturnCredits");
        echo "', '";
        echo getLang("ReturnCreditsHelp");
        echo "')\" src=\"images/help.gif\" width=\"24\" height=\"16\" border=\"0\">
\t\t\t\t\t\t\t<div style=\"display:none\" id=\"returns5\"></div>
\t\t\t\t\t\t</td>
\t\t\t\t\t</tr>
\t\t\t\t</table>

\t\t\t\t<table width=\"100%\" class=\"Panel HideOnDisabled\">
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td class=\"Heading2\" colspan=\"2\">";
        // line 82
        echo getLang("ReturnsNotifications");
        echo "</td>
\t\t\t\t\t</tr>

\t\t\t\t\t<tr>
\t\t\t\t\t\t<td class=\"FieldLabel\">
\t\t\t\t\t\t\t";
        // line 87
        echo getLang("NotifyOnReturn");
        echo ":
\t\t\t\t\t\t</td>
\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t<label><input type=\"checkbox\" name=\"returnotifyowner\" id=\"returnotifyowner\" value=\"1\" ";
        // line 90
        echo twig_safe_filter((isset($context['IsReturnNotifyOwner']) ? $context['IsReturnNotifyOwner'] : null));
        echo " /> ";
        echo getLang("YesEnableReturnNotifyOwner");
        echo "</label><br />
\t\t\t\t\t\t\t<label><input type=\"checkbox\" name=\"returnnotifycustomer\" id=\"returnnotifycustomer\" value=\"1\" ";
        // line 91
        echo twig_safe_filter((isset($context['IsReturnNotifyCustomer']) ? $context['IsReturnNotifyCustomer'] : null));
        echo " /> ";
        echo getLang("YesEnableReturnNotifyCustomer");
        echo "</label><br />
\t\t\t\t\t\t\t<label><input type=\"checkbox\" name=\"returnnotifystatus\" id=\"returnnotifystatus\" value=\"1\" ";
        // line 92
        echo twig_safe_filter((isset($context['IsReturnNotifyStatusChange']) ? $context['IsReturnNotifyStatusChange'] : null));
        echo " /> ";
        echo getLang("YesEnableReturnNotifyStatusChange");
        echo "</label><br />
\t\t\t\t\t\t</td>
\t\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t\t<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"100%\" class=\"PanelPlain\">
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td width=\"200\" class=\"FieldLabel\">
\t\t\t\t\t\t\t&nbsp;
\t\t\t\t\t\t</td>
\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t<input class=\"FormButton\" type=\"submit\" value=\"";
        // line 102
        echo getLang("Save");
        echo "\">
\t\t\t\t\t\t\t<input type=\"reset\" value=\"";
        // line 103
        echo getLang("Cancel");
        echo "\" class=\"FormButton\" onclick=\"ConfirmCancel()\" />
\t\t\t\t\t\t</td>
\t\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t</td>
\t\t</tr>
\t\t</table>
\t\t</div>
\t</form>

\t<script type=\"text/javascript\">
\t\tfunction ConfirmCancel() {
\t\t\tif(confirm('";
        // line 115
        echo getLang("ConfirmCancelSettings");
        echo "')) {
\t\t\t\tdocument.location.href='index.php?ToDo=viewReturnsSettings';
\t\t\t}
\t\t\telse {
\t\t\t\treturn false;
\t\t\t}
\t\t}

\t\tfunction CheckReturnsSettingsForm() {
\t\t\tif(\$('enablereturns').get().checked == true && \$('#returnreasons').val() == \"\") {
\t\t\t\talert('";
        // line 125
        echo getLang("EnterReturnReason");
        echo "');
\t\t\t\t\$('#returnreasons').focus();
\t\t\t\t\$('#returnreasons').select();
\t\t\t\treturn false;
\t\t\t}

\t\t\treturn true;
\t\t}

\t\tfunction ToggleReturnsStatus(status) {
\t\t\tif(status == true) {
\t\t\t\t\$('.HideOnDisabled').show();
\t\t\t}
\t\t\telse {
\t\t\t\t\$('.HideOnDisabled').hide();
\t\t\t}
\t\t}

\t\t\$(document).ready(function () {
\t\t\tif (\$('#enablereturns').attr('checked') == true) {
\t\t\t\t\$('.HideOnDisabled').show();
\t\t\t} else {
\t\t\t\t\$('.HideOnDisabled').hide();
\t\t\t}
\t\t});
\t</script>
";
    }

}
