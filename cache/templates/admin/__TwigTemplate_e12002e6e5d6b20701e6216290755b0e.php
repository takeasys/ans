<?php

/* settings.emailintegration.fieldsyncform.tpl */
class __TwigTemplate_e12002e6e5d6b20701e6216290755b0e extends Twig_Template
{
    public function display(array $context)
    {
        // line 1
        $context['formBuilder'] = $this->env->loadTemplate("macros/forms.tpl", true);
        // line 2
        $context['util'] = $this->env->loadTemplate("macros/util.tpl", true);
        echo "
<div id=\"ModalTitle\">Sync Order Fields</div>

<div id=\"ModalContent\">
\t<div class=\"emailIntegrationFieldSyncFormMessages\">
\t\t<div id=\"emailIntegrationFieldSyncFormDuplicateMessage\"></div>
\t\t<div id=\"emailIntegrationFieldSyncFormUnmatchedMessage\"></div>
\t</div>

\t";
        // line 12
        $this->env->loadTemplate("emailintegration.fieldsyncform.modalcontent.tpl")->display($context);
        echo "</div>

<div id=\"ModalButtonRow\">
\t";
        // line 16
        if (twig_length_filter($this->env, (isset($context['listFields']) ? $context['listFields'] : null))) {
            echo "\t\t<div style=\"float:left;\">
\t\t\t<input type=\"checkbox\" id=\"fieldSyncFormGuessFields\" /> <label for=\"fieldSyncFormGuessFields\">";
            // line 18
            echo getLang("FieldSyncFormGuessFieldsLabel", array("provider" => $this->getAttribute((isset($context['module']) ? $context['module'] : null), "name", array(), "any")));
            // line 20
            echo "</label>
\t\t</div>
\t";
        }
        // line 22
        echo "\t<div style=\"float:right;\">
\t\t<input type=\"button\" class=\"Button emailIntegrationFieldSyncFormCancelButton\" value=\"";
        // line 24
        echo getLang("Cancel");
        echo "\" />
\t\t<input type=\"button\" ";
        // line 25
        if ((!twig_length_filter($this->env, (isset($context['listFields']) ? $context['listFields'] : null)))) {
            echo "disabled=\"disabled\"";
        }
        echo " class=\"Submit emailIntegrationFieldSyncFormSubmitButton\" value=\"";
        echo getLang("Save");
        echo "\" />
\t</div>
\t<br style=\"clear:both;\" />
</div>
";
    }

}
