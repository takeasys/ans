<?php

	// Last Updated: 4th Apr 2018 @ 1:14 AM

	$GLOBALS['ISC_CFG']["isSetup"] = true;
	$GLOBALS['ISC_CFG']["Language"] = 'en';
	$GLOBALS['ISC_CFG']["AllowPurchasing"] = 1;
	$GLOBALS['ISC_CFG']["serverStamp"] = 'cctvvs';
	$GLOBALS['ISC_CFG']["HostingProvider"] = '';
	$GLOBALS['ISC_CFG']["UseWYSIWYG"] = 1;
	$GLOBALS['ISC_CFG']["dbType"] = 'mysql';
	$GLOBALS['ISC_CFG']["dbEncoding"] = 'UTF8';
	$GLOBALS['ISC_CFG']["dbServer"] = 'localhost';
	$GLOBALS['ISC_CFG']["dbUser"] = 'ansprotein_newe';
	$GLOBALS['ISC_CFG']["dbPass"] = 's0wQKBrY';
	$GLOBALS['ISC_CFG']["dbDatabase"] = 'ansprotein_newe';
	$GLOBALS['ISC_CFG']["tablePrefix"] = 'isc_';
	$GLOBALS['ISC_CFG']["StoreName"] = 'ANSBABY';
	$GLOBALS['ISC_CFG']["StoreAddress"] = '.';
	$GLOBALS['ISC_CFG']["LogoType"] = 'image';
	$GLOBALS['ISC_CFG']["StoreLogo"] = 'logo.png';
	$GLOBALS['ISC_CFG']["ShopPath"] = 'https://ansbaby.com';
	$GLOBALS['ISC_CFG']["CharacterSet"] = 'UTF-8';
	$GLOBALS['ISC_CFG']["HomePagePageTitle"] = '';
	$GLOBALS['ISC_CFG']["MetaKeywords"] = '';
	$GLOBALS['ISC_CFG']["MetaDesc"] = '';
	$GLOBALS['ISC_CFG']["DownloadDirectory"] = 'product_downloads';
	$GLOBALS['ISC_CFG']["ImageDirectory"] = 'product_images';
	$GLOBALS['ISC_CFG']["template"] = 'ansbaby';
	$GLOBALS['ISC_CFG']["SiteColor"] = 'default';
	$GLOBALS['ISC_CFG']["CurrencyToken"] = 'AU$';
	$GLOBALS['ISC_CFG']["CurrencyLocation"] = 'left';
	$GLOBALS['ISC_CFG']["DecimalToken"] = '.';
	$GLOBALS['ISC_CFG']["DecimalPlaces"] = 2;
	$GLOBALS['ISC_CFG']["ThousandsToken"] = ',';
	$GLOBALS['ISC_CFG']["InstallDate"] = 1363440401;

	// SSL Settings
	$GLOBALS['ISC_CFG']["UseSSL"] = '1';
	$GLOBALS['ISC_CFG']["SharedSSLPath"] = '';
	$GLOBALS['ISC_CFG']["SubdomainSSLPath"] = '';
	$GLOBALS['ISC_CFG']["ForceControlPanelSSL"] = 1;

	// Physical Dimensions Settings
	$GLOBALS['ISC_CFG']["WeightMeasurement"] = 'KGS';
	$GLOBALS['ISC_CFG']["LengthMeasurement"] = 'Centimeters';
	$GLOBALS['ISC_CFG']["DimensionsDecimalToken"] = '.';
	$GLOBALS['ISC_CFG']["DimensionsDecimalPlaces"] = '2';
	$GLOBALS['ISC_CFG']["DimensionsThousandsToken"] = ',';

	$GLOBALS['ISC_CFG']["DisplayDateFormat"] = 'jS M Y';
	$GLOBALS['ISC_CFG']["ExportDateFormat"] = 'jS M Y';
	$GLOBALS['ISC_CFG']["ExtendedDisplayDateFormat"] = 'jS M Y @ g:i A';
	$GLOBALS['ISC_CFG']["HomeFeaturedProducts"] = 4;
	$GLOBALS['ISC_CFG']["HomeNewProducts"] = 8;
	$GLOBALS['ISC_CFG']["HomeBlogPosts"] = 10;
	$GLOBALS['ISC_CFG']["CategoryProductsPerPage"] = 16;
	$GLOBALS['ISC_CFG']["CategoryListDepth"] = 1;
	$GLOBALS['ISC_CFG']["ProductReviewsPerPage"] = 10;
	$GLOBALS['ISC_CFG']["TagCloudsEnabled"] = 1;
	$GLOBALS['ISC_CFG']["ShowAddToCartQtyBox"] = 1;
	$GLOBALS['ISC_CFG']["CaptchaEnabled"] = 1;
	$GLOBALS['ISC_CFG']["ShowCartSuggestions"] = 1;
	$GLOBALS['ISC_CFG']["AdminEmail"] = 'tomlashan@ansbaby.com';
	$GLOBALS['ISC_CFG']["OrderEmail"] = 'tomlashan@ansbaby.com';
	$GLOBALS['ISC_CFG']['LowInventoryNotificationAddress'] = '';
	$GLOBALS['ISC_CFG']["ShowThumbsInCart"] = 1;
	$GLOBALS['ISC_CFG']["AutoApproveReviews"] = 0;
	$GLOBALS['ISC_CFG']["SearchSuggest"] = 1;
	$GLOBALS['ISC_CFG']["QuickSearch"] = 1;

	// Shipping Settings
	$GLOBALS['ISC_CFG']["CompanyName"] = 'ANS BABY';
	$GLOBALS['ISC_CFG']["CompanyAddress"] = 'Level 18, Wheelock House, 20 Pedder Street, Central';
	$GLOBALS['ISC_CFG']["CompanyCity"] = 'Hong Kong';
	$GLOBALS['ISC_CFG']["CompanyCountry"] = 'Hong Kong';
	$GLOBALS['ISC_CFG']["CompanyState"] = 'Hong Kong';
	$GLOBALS['ISC_CFG']["CompanyZip"] = 'N/A';

	// Checkout Settings
	$GLOBALS['ISC_CFG']["CheckoutMethods"] = 'checkout_paypal';
	$GLOBALS['ISC_CFG']['CheckoutType'] = 'single';
	$GLOBALS['ISC_CFG']['GuestCheckoutEnabled'] = 1;
	$GLOBALS['ISC_CFG']['GuestCheckoutCreateAccounts'] = 1;

	$GLOBALS['ISC_CFG']["EmailIntegrationMethods"] = 'emailintegration_mailchimp';
	$GLOBALS['ISC_CFG']["EmailIntegrationNewsletterDoubleOptin"] = 1;
	$GLOBALS['ISC_CFG']["EmailIntegrationNewsletterSendWelcome"] = 1;
	$GLOBALS['ISC_CFG']["EmailIntegrationOrderDoubleOptin"] = 1;
	$GLOBALS['ISC_CFG']["EmailIntegrationOrderSendWelcome"] = 1;

	$GLOBALS['ISC_CFG']["ShowThumbsInControlPanel"] = 1;
	$GLOBALS['ISC_CFG']["EnableSEOUrls"] = 2;
	$GLOBALS['ISC_CFG']['ShowInventory'] = 0;
	$GLOBALS['ISC_CFG']['ShowPreOrderInventory'] = 0;
	$GLOBALS['ISC_CFG']['DefaultPreOrderMessage'] = 'Expected release date is %%DATE%%';
	$GLOBALS['ISC_CFG']['StoreTimeZone'] = '10';
	$GLOBALS['ISC_CFG']['StoreDSTCorrection'] = 0;
	$GLOBALS['ISC_CFG']['ShowDownloadTemplates'] = '1';
	$GLOBALS['ISC_CFG']['TagCartQuantityBoxes'] = 'dropdown';
	$GLOBALS['ISC_CFG']['ProductBreadcrumbs'] = 'showall';
	$GLOBALS['ISC_CFG']['FastCartAction'] = 'popup';

	$GLOBALS['ISC_CFG']["RSSNewProducts"] = 1;
	$GLOBALS['ISC_CFG']["RSSPopularProducts"] = 1;
	$GLOBALS['ISC_CFG']["RSSFeaturedProducts"] = 1;
	$GLOBALS['ISC_CFG']["RSSCategories"] = 1;
	$GLOBALS['ISC_CFG']["RSSProductSearches"] = 1;
	$GLOBALS['ISC_CFG']["RSSLatestBlogEntries"] = 1;
	$GLOBALS['ISC_CFG']["RSSItemsLimit"] = 10;
	$GLOBALS['ISC_CFG']["RSSCacheTime"] = 60;
	$GLOBALS['ISC_CFG']["RSSSyndicationIcons"] = 1;

	$GLOBALS['ISC_CFG']['BackupsLocal'] = 1;
	$GLOBALS['ISC_CFG']['BackupsRemoteFTP'] = 0;
	$GLOBALS['ISC_CFG']['BackupsRemoteFTPHost'] = '';
	$GLOBALS['ISC_CFG']['BackupsRemoteFTPUser'] = '';
	$GLOBALS['ISC_CFG']['BackupsRemoteFTPPass'] = '';
	$GLOBALS['ISC_CFG']['BackupsRemoteFTPPath'] = '';
	$GLOBALS['ISC_CFG']['BackupsAutomatic'] = 0;
	$GLOBALS['ISC_CFG']['BackupsAutomaticMethod'] = 'local';
	$GLOBALS['ISC_CFG']['BackupsAutomaticDatabase'] = 1;
	$GLOBALS['ISC_CFG']['BackupsAutomaticImages'] = 1;
	$GLOBALS['ISC_CFG']['BackupsAutomaticDownloads'] = 1;

	$GLOBALS['ISC_CFG']["GoogleMapsAPIKey"] = '';
	$GLOBALS['ISC_CFG']["NotificationMethods"] = 'notification_email';
	$GLOBALS['ISC_CFG']["CurrencyMethods"] = 'currency_webservicex';
	$GLOBALS['ISC_CFG']["DefaultCurrencyID"] = 1;
	$GLOBALS['ISC_CFG']["DefaultCurrencyRate"] = 1;

	$GLOBALS['ISC_CFG']["MailAutomaticallyTickNewsletterBox"] = 1;
	$GLOBALS['ISC_CFG']["MailAutomaticallyTickOrderBox"] = 1;
	$GLOBALS['ISC_CFG']['ShowMailingListInvite'] = 1;

	$GLOBALS['ISC_CFG']["AnalyticsMethods"] = 'analytics_googleanalytics';

	$GLOBALS['ISC_CFG']['SystemLogging'] = 1;
	$GLOBALS['ISC_CFG']['HidePHPErrors'] = 0;
	$GLOBALS['ISC_CFG']['SystemLogTypes'] = 'general,payment,shipping,notification,sql,php,accounting,emailintegration,ebay';
	$GLOBALS['ISC_CFG']['SystemLogSeverity'] = 'errors,warnings,success,notices';
	$GLOBALS['ISC_CFG']['SystemLogMaxLength'] = 1000;
	$GLOBALS['ISC_CFG']['AdministratorLogging'] = 1;
	$GLOBALS['ISC_CFG']['AdministratorLogMaxLength'] = 1000;
	$GLOBALS['ISC_CFG']['DebugMode'] = 0;

	$GLOBALS['ISC_CFG']['EnableReturns'] = 1;
	$GLOBALS['ISC_CFG']['ReturnReasons'] = array (
  0 => 'Received Wrong Product',
  1 => 'Wrong Product Ordered',
  2 => 'Not Satisfied With The Product',
  3 => 'There Was A Problem With The Product',
);
	$GLOBALS['ISC_CFG']['ReturnActions'] = array (
  0 => 'Repair',
  1 => 'Replacement',
  2 => 'Store Credit',
);
	$GLOBALS['ISC_CFG']['ReturnCredits'] = 1;
	$GLOBALS['ISC_CFG']['ReturnInstructions'] = '';
	$GLOBALS['ISC_CFG']['EmailOwnerOnReturn'] = 1;
	$GLOBALS['ISC_CFG']['SendReturnConfirmation'] = 1;
	$GLOBALS['ISC_CFG']['NotifyOnReturnStatusChange'] = 1;

	$GLOBALS['ISC_CFG']['EnableGiftCertificates'] = 1;
	$GLOBALS['ISC_CFG']['GiftCertificateAmounts'] = array (
);
	$GLOBALS['ISC_CFG']['GiftCertificateCustomAmounts'] = 1;
	$GLOBALS['ISC_CFG']['GiftCertificateMinimum'] = 1;
	$GLOBALS['ISC_CFG']['GiftCertificateMaximum'] = 1000;
	$GLOBALS['ISC_CFG']['GiftCertificateExpiry'] = 0;
	$GLOBALS['ISC_CFG']['GiftCertificateThemes'] = 'Birthday.html,Boy.html,Celebration.html,Christmas.html,General.html,Girl.html';
	$GLOBALS['ISC_CFG']['GiftCertificateCustomDirectory'] = '__gift_themes';
	$GLOBALS['ISC_CFG']['GiftCertificateMasterDirectory'] = '__master/__gift_themes';

	$GLOBALS['ISC_CFG']['UpdateInventoryLevels'] = 1;
	$GLOBALS['ISC_CFG']['UpdateInventoryOnOrderEdit'] = 1;
	$GLOBALS['ISC_CFG']['UpdateInventoryOnOrderDelete'] = 1;
	$GLOBALS['ISC_CFG']['OrderStatusNotifications'] = '11,9,8,3,10,2,4';

	$GLOBALS['ISC_CFG']['AddonModules'] = '';

	$GLOBALS['ISC_CFG']['AKBIsConfigured'] = '0';
	$GLOBALS['ISC_CFG']['AKBPath'] = '';
	$GLOBALS['ISC_CFG']['ARSPageIds'] = '';
	$GLOBALS['ISC_CFG']['ARSIntegrated'] = '';

	$GLOBALS['ISC_CFG']['ShowProductPrice'] = 1;
	$GLOBALS['ISC_CFG']['ShowProductSKU'] = 1;
	$GLOBALS['ISC_CFG']['ShowProductWeight'] = 1;
	$GLOBALS['ISC_CFG']['ShowProductBrand'] = 1;
	$GLOBALS['ISC_CFG']['ShowProductShipping'] = 1;
	$GLOBALS['ISC_CFG']['ShowProductRating'] = 0;
	$GLOBALS['ISC_CFG']['ProductImageMode'] = 'lightbox';

	$GLOBALS['ISC_CFG']['ShowAddThisLink'] = 1;

	// DO NOT CHANGE THIS VARIABLE OR YOU WILL BREAK ORDERS
	$GLOBALS['ISC_CFG']["EncryptionToken"] = '26359a146dfe8d4e04ccae28922cd561';

	$GLOBALS['ISC_CFG']["EnableWishlist"] = 0;
	$GLOBALS['ISC_CFG']["EnableAccountCreation"] = 1;
	$GLOBALS['ISC_CFG']['EnableProductComparisons'] = 0;
	$GLOBALS['ISC_CFG']["EnableOrderComments"] = 1;
	$GLOBALS['ISC_CFG']["EnableOrderTermsAndConditions"] = 0;
	$GLOBALS['ISC_CFG']["OrderTermsAndConditionsType"] = '';
	$GLOBALS['ISC_CFG']["OrderTermsAndConditionsLink"] = '';
	$GLOBALS['ISC_CFG']["OrderTermsAndConditions"] = '';

	// Logo Settings
	$GLOBALS['ISC_CFG']["LogoFields"] = array (
);
	$GLOBALS['ISC_CFG']["ForceWebsiteTitleText"] = 0;
	$GLOBALS['ISC_CFG']['UseAlternateTitle'] = 1;
	$GLOBALS['ISC_CFG']['AlternateTitle'] = '';
	$GLOBALS['ISC_CFG']['UsingLogoEditor'] = 0;
	$GLOBALS['ISC_CFG']['UsingTemplateLogo'] = 0;

	$GLOBALS['ISC_CFG']['AffiliateConversionTrackingCode'] = '';

	$GLOBALS['ISC_CFG']['GuestCustomerGroup'] = 0;
	$GLOBALS['ISC_CFG']['ForwardInvoiceEmails'] = 'stevechoi@ansgo.com.hk';

	// Mail Settings
	$GLOBALS['ISC_CFG']['MailUseSMTP'] = 0;
	$GLOBALS['ISC_CFG']['MailSMTPServer'] = '';
	$GLOBALS['ISC_CFG']['MailSMTPUsername'] = '';
	$GLOBALS['ISC_CFG']['MailSMTPPassword'] = '';
	$GLOBALS['ISC_CFG']['MailSMTPPort'] = '';

	// Curl Proxy Settings
	$GLOBALS['ISC_CFG']['HTTPProxyServer'] = '';
	$GLOBALS['ISC_CFG']['HTTPProxyPort'] = '';
	$GLOBALS['ISC_CFG']['HTTPSSLVerifyPeer'] = 0;

	// Digital Download Settings
	$GLOBALS['ISC_CFG']['DigitalOrderHandlingFee'] = 0;

	// Accounting Settings
	$GLOBALS['ISC_CFG']['AccountingMethods'] = '';

	// Live Chat Modules
	$GLOBALS['ISC_CFG']['LiveChatModules'] = '';

	//Category and Brand image dimensions
	$GLOBALS['ISC_CFG']['CategoryPerRow'] = 3;
	$GLOBALS['ISC_CFG']['CategoryImageWidth'] = 120;
	$GLOBALS['ISC_CFG']['CategoryImageHeight'] = 120;
	$GLOBALS['ISC_CFG']['CategoryDefaultImage'] = '';
	$GLOBALS['ISC_CFG']['BrandPerRow'] = 3;
	$GLOBALS['ISC_CFG']['BrandImageWidth'] = 120;
	$GLOBALS['ISC_CFG']['BrandImageHeight'] = 120;
	$GLOBALS['ISC_CFG']['BrandDefaultImage'] = '';

	// Product Images
	$GLOBALS['ISC_CFG']['DefaultProductImage'] = 'template';

	//Display the 'Add to Cart' link on all the product panels
	$GLOBALS['ISC_CFG']['ShowAddToCartLink'] = 1;

	$GLOBALS['ISC_CFG']['CategoryListingMode'] = 'emptychildren';
	$GLOBALS['ISC_CFG']['CategoryDisplayMode'] = 'grid';
	$GLOBALS['ISC_CFG']['TagCloudMinSize'] = 80;
	$GLOBALS['ISC_CFG']['TagCloudMaxSize'] = 300;

	// Bulk Discounts
	$GLOBALS['ISC_CFG']['BulkDiscountEnabled'] = 1;

	$GLOBALS['ISC_CFG']['EnableProductTabs'] = 0;

	$GLOBALS['ISC_CFG']['MultipleShippingAddresses'] = 1;

	// Vendor Edition Settings
	$GLOBALS['ISC_CFG']['VendorLogoSize'] = '120x120';
	$GLOBALS['ISC_CFG']['VendorPhotoSize'] = '120x120';

	// The factoring dimension for a shipping quote (depth, height or width with default of depth)
	$GLOBALS['ISC_CFG']['ShippingFactoringDimension'] = 'depth';

	// Array of the getting started steps that have been completed
	$GLOBALS['ISC_CFG']['GettingStartedCompleted'] = array (
  0 => 'design',
  1 => 'settings',
  2 => 'taxSettings',
  3 => 'products',
  4 => 'paymentMethods',
  5 => 'shippingOptions',
  6 => 'storeComplete',
);

	// The favicon file
	$GLOBALS['ISC_CFG']['Favicon'] = 'favicon.ico';

	// Session settings
	$GLOBALS['ISC_CFG']['SessionSavePath'] = '';

	// Optimizer Settings
	$GLOBALS['ISC_CFG']['OptimizerMethods'] = array (
);

	// Advance Search format (search all)
	$GLOBALS['ISC_CFG']['SearchDefaultProductSort'] = 'relevance';
	$GLOBALS['ISC_CFG']['SearchDefaultContentSort'] = 'relevance';
	$GLOBALS['ISC_CFG']['SearchProductDisplayMode'] = 'list';
	$GLOBALS['ISC_CFG']['SearchResultsPerPage'] = 16;
	$GLOBALS['ISC_CFG']['SearchOptimisation'] = 'fulltext';

	// Abandon Orders
	$GLOBALS['ISC_CFG']['AbandonOrderLifetime'] = '30';

	// Product Image Settings
	$GLOBALS['ISC_CFG']['ProductImagesStorewideThumbnail_width'] = 327;
	$GLOBALS['ISC_CFG']['ProductImagesStorewideThumbnail_height'] = 470;
	$GLOBALS['ISC_CFG']['ProductImagesStorewideThumbnail_timeChanged'] = 1455521721;
	$GLOBALS['ISC_CFG']['ProductImagesProductPageImage_width'] = 420;
	$GLOBALS['ISC_CFG']['ProductImagesProductPageImage_height'] = 420;
	$GLOBALS['ISC_CFG']['ProductImagesProductPageImage_timeChanged'] = 1455675945;
	$GLOBALS['ISC_CFG']['ProductImagesGalleryThumbnail_width'] = 30;
	$GLOBALS['ISC_CFG']['ProductImagesGalleryThumbnail_height'] = 30;
	$GLOBALS['ISC_CFG']['ProductImagesGalleryThumbnail_timeChanged'] = 0;
	$GLOBALS['ISC_CFG']['ProductImagesZoomImage_width'] = 1280;
	$GLOBALS['ISC_CFG']['ProductImagesZoomImage_height'] = 1280;
	$GLOBALS['ISC_CFG']['ProductImagesZoomImage_timeChanged'] = 0;
	$GLOBALS['ISC_CFG']['ProductImagesTinyThumbnailsEnabled'] = 1;
	$GLOBALS['ISC_CFG']['ProductImagesImageZoomEnabled'] = 1;

	// Variable used to force browsers to re-download already cached
	// stylesheets/Javascript. Set to a random value during the upgrade.
	$GLOBALS['ISC_CFG']['JSCacheToken'] = 1;

	// Shopping Comparison
	$GLOBALS['ISC_CFG']['ShoppingComparisonModules'] = '';

	// Maintenance
	$GLOBALS['ISC_CFG']["DownForMaintenance"] = 0;
	$GLOBALS['ISC_CFG']["DownForMaintenanceMessage"] = '';

	// Starting Order Number
	$GLOBALS['ISC_CFG']["StartingOrderNumber"] = 108;

	// Shipping Manager Settings
	$GLOBALS['ISC_CFG']['ShippingManagerModules'] = '';

	// 'Customers who viewed this product also viewed' Settings
	$GLOBALS['ISC_CFG']['EnableCustomersAlsoViewed'] = 1;
	$GLOBALS['ISC_CFG']['CustomersAlsoViewedCount'] = 4;

	// Ebay Settings
	$GLOBALS['ISC_CFG']['EbayDevId'] = '';
	$GLOBALS['ISC_CFG']['EbayAppId'] = '';
	$GLOBALS['ISC_CFG']['EbayCertId'] = '';
	$GLOBALS['ISC_CFG']['EbayUserToken'] = '';
	$GLOBALS['ISC_CFG']['EbayDefaultSite'] = '';
	$GLOBALS['ISC_CFG']['EbayStore'] = '';
	$GLOBALS['ISC_CFG']['EbayTestMode'] = '';
	$GLOBALS['ISC_CFG']['EbaySettingsValid'] = false;

	// Comment System Settings
	$GLOBALS['ISC_CFG']['CommentSystemModule'] = 'comments_builtincomments';

	// Redirect to www or no www
	$GLOBALS['ISC_CFG']['RedirectWWW'] = '0';

	// Tax Settings
	$GLOBALS['ISC_CFG']['taxLabel'] = 'Tax';
	$GLOBALS['ISC_CFG']['taxEnteredWithPrices'] = '0';
	$GLOBALS['ISC_CFG']['taxCalculationBasedOn'] = '0';
	$GLOBALS['ISC_CFG']['taxDefaultTaxDisplayCatalog'] = '0';
	$GLOBALS['ISC_CFG']['taxDefaultTaxDisplayProducts'] = '0';
	$GLOBALS['ISC_CFG']['taxDefaultTaxDisplayCart'] = '0';
	$GLOBALS['ISC_CFG']['taxDefaultTaxDisplayOrders'] = '0';
	$GLOBALS['ISC_CFG']['taxChargesOnOrdersBreakdown'] = '0';
	$GLOBALS['ISC_CFG']['taxChargesInCartBreakdown'] = '0';
	$GLOBALS['ISC_CFG']['taxDefaultCountry'] = '96';
	$GLOBALS['ISC_CFG']['taxDefaultState'] = '0';
	$GLOBALS['ISC_CFG']['taxDefaultZipCode'] = '000000';
	$GLOBALS['ISC_CFG']['taxPendingChanges'] = array (
  'rebuildPricing' => 
  array (
    0 => 0,
  ),
);
	$GLOBALS['ISC_CFG']['taxShippingTaxClass'] = '2';
	$GLOBALS['ISC_CFG']['taxGiftWrappingTaxClass'] = '3';
	$GLOBALS['ISC_CFG']['taxPendingChanges'] = array (
  'rebuildPricing' => 
  array (
    0 => 0,
  ),
);

	// PCI config
	$GLOBALS['ISC_CFG']['PCIPasswordMinLen'] = 0;
	$GLOBALS['ISC_CFG']['PCIPasswordHistoryCount'] = 0;
	$GLOBALS['ISC_CFG']['PCIPasswordExpiryTimeDay'] = 0;
	$GLOBALS['ISC_CFG']['PCILoginAttemptCount'] = 0;
	$GLOBALS['ISC_CFG']['PCILoginLockoutTimeMin'] = 0;
	$GLOBALS['ISC_CFG']['PCILoginIdleTimeMin'] = 0;
	$GLOBALS['ISC_CFG']['PCILoginInactiveTimeDay'] = 0;

	// Mobile/Portable Template
	$GLOBALS['ISC_CFG']['enableMobileTemplate'] = true;
	$GLOBALS['ISC_CFG']['enableMobileTemplateDevices'] = array (
  0 => 'iphone',
  1 => 'ipod',
  2 => 'pre',
  3 => 'android',
);
	$GLOBALS['ISC_CFG']['mobileTemplateLogo'] = 'mobile_logo.png';

	// Facebook Like Button
	$GLOBALS['ISC_CFG']['FacebookLikeButtonEnabled'] = 0;
	$GLOBALS['ISC_CFG']['FacebookLikeButtonStyle'] = 'standard';
	$GLOBALS['ISC_CFG']['FacebookLikeButtonPosition'] = 'below';
	$GLOBALS['ISC_CFG']['FacebookLikeButtonVerb'] = 'recommend';
	$GLOBALS['ISC_CFG']['FacebookLikeButtonShowFaces'] = 1;
	$GLOBALS['ISC_CFG']['FacebookLikeButtonAdminIds'] = '';

	// Deleted orders handling
	$GLOBALS['ISC_CFG']['DeletedOrdersAction'] = 'delete';

	// Category flyout menu configuration
	$GLOBALS['ISC_CFG']['CategoryListStyle'] = 'static';
	$GLOBALS['ISC_CFG']['categoryFlyoutMouseOutDelay'] = 0.8;
	$GLOBALS['ISC_CFG']['categoryFlyoutDropShadow'] = 0;
