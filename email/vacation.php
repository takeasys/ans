<?php
require_once("login.php");

include("include/header.php");


$startstamp=time();
$endstamp=time();
$text="";


$time_of_day = array( "morning" => "Morning", "afternoon" => "Afternoon", "evening" => "Evening");
$months = array();
for ($i=1; $i<=12; $i++)
{
	$months[$i] = date("M", mktime(0,0,0,$i,1,0));
}
$days = array();
for ($i=1; $i<=31; $i++)
{
	$days[$i] = $i;
}

$start_year = 2007;
$end_year = 2029;
$years = array();
for ($i=$start_year; $i<=$end_year; $i++)
{
        $years[$i] = $i;
}

if (isset($_POST['action']) && ($_POST['action'] == "create" || $_POST['action'] == "modify" || $_POST['action'] == "delete"))
{
	$sock = newSock();
	$sock->query('/CMD_EMAIL_ACCOUNT_VACATION',
        array(
                'user' => $_SESSION['user'],
		'domain'=>$_SESSION['domain'],
                'password' => $_SESSION['password'],
                'action' => $_POST['action'],
		'text' => stripslashes($_POST['text']),
		'starttime' => $_POST['starttime'],
		'startmonth' => $_POST['startmonth'],
		'startday' => $_POST['startday'],
		'startyear' => $_POST['startyear'],
		'endtime' => $_POST['endtime'],
		'endmonth' => $_POST['endmonth'],
		'endday' => $_POST['endday'],
		'endyear' => $_POST['endyear']		
         ));

	$result = $sock->fetch_parsed_body();

	if ( $result['error'] != "0" )
	{
        	no_go("Unable to set vacation message:<br><b>".$result['text']."</b>");
	}

	echo "Vacation message ";
	switch ($_POST['action'])
	{	case "create" : echo "created"; break;
		case "modify" : echo "updated"; break;
		case "delete" : echo "deleted"; break;
	}
	echo ".<br><br><a href='index.php'>Click here</a> to go back.";

	include("include/footer.php");
	exit(0);
}

//grab any vacation message if there is one.
$sock1 = newSock();
$sock1->query('/CMD_EMAIL_ACCOUNT_VACATION',
        array(
                'user' => $_SESSION['user'],
                'domain'=>$_SESSION['domain'],
                'password' => $_SESSION['password'],
        ));

$result1 = $sock1->fetch_parsed_body();

$exists=0;

if ($result1 == 0)
{
        no_go("socket retuned a zero result");
}

if ( $result1['error'] == "0" )
{
        if (!isset($result1['startyear']))
        {
                no_go("socket returned no error, but there is data missing.  Try reloading this page.");
        }

        $startstamp=getTimeFromVars($result1['startyear'], $result1['startmonth'], $result1['startday'], $result1['starttime']);
        $endstamp=getTimeFromVars($result1['endyear'], $result1['endmonth'], $result1['endday'], $result1['endtime']);
        $text=$result1['text'];
        $exists=1;

        //print_r($result1);
        //echo date("r", $startstamp)."<br><br>\n\n";

        //echo $sock1->fetch_body();

}
else
{
        //echo, I guess it doesn't exist yet.
        //echo "not there!<br>";
        //print_r($result1);
}

?>

<br>
<h1>Set Vacation Messsage</h1>

<table cellpadding=3 cellspacing=1>
<form action="?" method="POST">
<input type=hidden name="action" value="<?php if ($exists) { echo "modify";}else{echo "create";}?>">
<tr><td>Vacation Message:</td><td class=list align=center><textarea rows=15 cols=60 name=text><?php echo $text;?></textarea></td></tr>
<tr><td>Vacation Start: </td><td><?php showTime("start", $startstamp); ?></td></tr>
<tr><td>Vacation End: </td><td><?php showTime("end", $endstamp); ?></td></tr>
<tr><td>Current Server Time:</td><td><?php echo get_tod(time())." of ".date("M j, Y"); ?></td></tr>
<tr><td colspan=2 align=center><input type=submit value="<?php if($exists){echo "Update";}else{echo "Set";}?> Vacation Message"></td></tr>

</form>
</table>

<?php if ($exists) { ?>
<br><br>
<form action="?" method="POST">
<input type=hidden name="action" value="delete">
<input type=submit value="Delete current Vacation Message">
</form>
<?php
}


include("include/footer.php");

function show_select($name, $arr, $selected='')
{

	echo "<select name='$name'>";
	foreach($arr as $v => $t)
	{
		if ($v == $selected)
		{
			echo "\t<option selected value='$v'>$t</option>\n";
		}
		else
		{
			echo "\t<option value='$v'>$t</option>\n";
		}
	}
	echo "</select>\n";

}

function get_tod($stamp)
{
	$hour = (int)date("H", $stamp);

	if (0 <= $hour && $hour < 12) { return "morning"; }
	if (12 <= $hour && $hour < 18) { return "afternoon"; }
	return "evening";
}

function getTimeFromVars($year, $month, $day, $tod)
{
	switch($tod)
	{
		case "morning" : $hour = 6; break;
		case "afternoon" : $hour = 12; break;
		case "evening" : $hour = 18; break;
		default : $hour = 0; break;
	}

	return mktime($hour, 0, 0, $month, $day, $year);
}

function showTime($prefix, $stamp=0)
{
	global $time_of_day, $months, $days, $years;

	show_select("${prefix}time", $time_of_day, get_tod($stamp));

	show_select("${prefix}month", $months, date("m", $stamp));

	show_select("${prefix}day", $days, date("j", $stamp));

	show_select("${prefix}year", $years, date("Y", $stamp));


}

?>
